require 'date'
require 'json'

namespace :abq_parking_citations do
  desc "Import citation data into database"
  task populate_citations: :environment do
    puts "populating citations table..."
    t1 = Time.now
    json_file = File.read('/Users/jasonloyd/rails_apps/abq_parking_citations/out_2.json')
    raw_citation_hash = JSON.parse(json_file)
    raw_citation_hash["features"].each do |c|
      tmp_id = c["attributes"]["OBJECTID"]
      if Citation.find_by_id(tmp_id).nil?
        new_citation_record = Citation.new
        new_geometry_record = Geometry.new
        new_citation_record.id = tmp_id 
        new_citation_record.citation_num = c["attributes"]["CitationNum"]
        new_citation_record.street_location = c["attributes"]["Street_Location"]
        new_citation_record.violation = c["attributes"]["Violation"]
        issue_date_posix_tmp = c["attributes"]["IssueDate"]
        issue_time_posix_tmp = c["attributes"]["IssueTime"]
        unless issue_date_posix_tmp.blank? or issue_time_posix_tmp.blank?
          issue_date_posix = issue_date_posix_tmp/1000 
          issue_time_posix = issue_time_posix_tmp/1000 
          issue_date_date = DateTime.strptime(issue_date_posix.to_s, '%s')
          issue_time_date = DateTime.strptime(issue_time_posix.to_s, '%s')
          unless issue_date_date.nil? or issue_time_date.nil?
            new_citation_record.issue_date = issue_date_date + Time.parse(issue_time_date.strftime("%H:%M:%S")).seconds_since_midnight.seconds 
          end
        end
        new_citation_record.save
        new_geometry_record.x = c["geometry"]["x"]
        new_geometry_record.y = c["geometry"]["y"]
        new_geometry_record.citation_id = new_citation_record.id 
        new_geometry_record.save
      end
    end
    t2 = Time.now
    time_delta = t2 - t1
    puts "done populating citations table!"
    puts "it took " + time_delta.to_s + " seconds"
    puts "(" + (time_delta/60).to_s + " minutes)"
    puts "(" + (time_delta/3600).to_s + " hours :-( )"
  end
end
