class CitationsController < ApplicationController
  before_action :set_citation, only: [:show, :edit, :update, :destroy]

  # GET /citations
  # GET /citations.json
  def index
    #@citations = Citation.all
    citations = Citation.issued_within_last_week
    unless params.nil?
      if params.has_key?(:time_scope)
        if params[:time_scope] == "Last Week"
          #Rails.logger.info "time scope => Last Week"
          citations = Citation.issued_within_last_week
        end
        if params[:time_scope] == "Last Month"
          #Rails.logger.info "time scope => Last Month"
          citations = Citation.issued_within_last_month
        end
        if params[:time_scope] == "Last 3 Months"
          #Rails.logger.info "time scope => Last 3 months"
          citations = Citation.issued_within_last_three_months
        end
        if params[:time_scope] == "Last 6 Months"
          #Rails.logger.info "time scope => Last 6 months"
          citations = Citation.issued_within_last_six_months
        end
        if params[:time_scope] == "Last 9 Months"
          #Rails.logger.info "time scope => Last 9 months"
          citations = Citation.issued_within_last_nine_months
        end
        if params[:time_scope] == "Last Year"
          #Rails.logger.info "time scope => Last Year"
          citations = Citation.issued_within_last_year
        end
      end
    end
    @citations = Citation.build_heat_map_json(citations)
    #Rails.logger.info("*****************")
    #Rails.logger.info "Total Citations: #{citations.count}"
    @citations
  end

  # GET /citations/1
  # GET /citations/1.json
  def show
  end

  # GET /citations/new
  def new
    @citation = Citation.new
  end

  # GET /citations/1/edit
  def edit
  end

  # POST /citations
  # POST /citations.json
  def create
    @citation = Citation.new(citation_params)

    respond_to do |format|
      if @citation.save
        format.html { redirect_to @citation, notice: 'Citation was successfully created.' }
        format.json { render :show, status: :created, location: @citation }
      else
        format.html { render :new }
        format.json { render json: @citation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /citations/1
  # PATCH/PUT /citations/1.json
  def update
    respond_to do |format|
      if @citation.update(citation_params)
        format.html { redirect_to @citation, notice: 'Citation was successfully updated.' }
        format.json { render :show, status: :ok, location: @citation }
      else
        format.html { render :edit }
        format.json { render json: @citation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /citations/1
  # DELETE /citations/1.json
  def destroy
    @citation.destroy
    respond_to do |format|
      format.html { redirect_to citations_url, notice: 'Citation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_citation
      @citation = Citation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def citation_params
      params.require(:citation).permit(:citation_num, :issue_date, :street_location, :violation, :geometry_id)
    end
end
