class Citation < ActiveRecord::Base
  has_one :Geometry

  def self.build_heat_map_json(citations_list)
    heat_map_arr = []
    day_count = {1 => {}, 2 => {}, 3 => {}, 4 => {}, 5 => {}, 6 => {}, 7 => {}}
    citations_list.each do |c|
      unless c.issue_date.blank?
        current_issue_date = c.issue_date
        current_wday = current_issue_date.wday + 1
        current_hour = current_issue_date.hour + 1
        unless day_count[current_wday][current_hour].nil?
          day_count[current_wday][current_hour] += 1
        else
          day_count[current_wday][current_hour] = 1
        end
      end
    end
    day_count.each do |k,v|
      v.each do |i,j|
        heat_map_arr << {"day" => k, "hour" => i, "value" => j}
      end
    end
    heat_map_arr
  end

  def self.issued_between(time1, time2)
    where("issue_date >= ? and issue_date <= ?", time1, time2)
  end

  def self.issued_since(time)
    where("issue_date >= ? and issue_date <= ?", time, Time.now)
  end

  def self.issued_within_last_week
    where("issue_date >= ?", 1.week.ago)
  end

  def self.issued_within_last_month
    where("issue_date >= ?", 1.month.ago)
  end

  def self.issued_within_last_three_months
    where("issue_date >= ?", 3.months.ago)
  end

  def self.issued_within_last_six_months
    where("issue_date >= ?", 6.months.ago)
  end

  def self.issued_within_last_nine_months
    where("issue_date >= ?", 9.months.ago)
  end

  def self.issued_within_last_year
    where("issue_date >= ?", 1.year.ago)
  end

end
