Rails.application.routes.draw do
  resources :citations
  root to: 'citations#index'
end
