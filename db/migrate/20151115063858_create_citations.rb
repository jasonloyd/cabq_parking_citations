class CreateCitations < ActiveRecord::Migration
  def change
    create_table :citations do |t|
      t.string :citation_num
      t.datetime :issue_date
      t.string :street_location
      t.string :violation

      t.timestamps null: false
    end
  end
end
