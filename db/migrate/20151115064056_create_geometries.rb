class CreateGeometries < ActiveRecord::Migration
  def change
    create_table :geometries do |t|
      t.float :x
      t.float :y
      t.integer :citation_id

      t.timestamps null: false
    end
  end
end
